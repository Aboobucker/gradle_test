 package testcases;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Learnextents {
	
	public void extents() throws IOException {
		
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports.result.html");
		html.setAppendExisting(false);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger=extent.createTest("TC001mergelead","create merge lead");
		
		
		logger.log(Status.PASS,"Data enterded successfully",MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		
	extent.flush();
		
	}

}
