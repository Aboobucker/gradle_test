package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week5.day2.Reports;

public class SeMethods extends Reports implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url)
	
	{
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
		   reportStep("PASS","The Browser \"+browser+\" Launched Successfully");
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "LinkText":return driver.findElementByLinkText(locValue);
			case "css": return driver.findElementByCssSelector(locValue);
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}
		return null;
	}
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementsById(locValue);
			case "class": return driver.findElementsByClassName(locValue);
			case "xpath": return driver.findElementsByXPath(locValue);
			case "LinkText":return driver.findElementsByLinkText(locValue);
			case "css": return driver.findElementsByCssSelector(locValue);
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is Entered Successfully");
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
		takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub

	}

}
