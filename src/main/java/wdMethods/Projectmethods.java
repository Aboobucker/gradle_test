package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class Projectmethods extends SeMethods {
	
	@BeforeSuite(groups="any")
	public void startTest() {
		startResult();
	}
	@BeforeClass(groups="any")
	public void data() {
		beforeMethod();
	}
	@Parameters({"url","uname","pwd"})
@BeforeMethod(groups="any")
	public void login(String url, String username,String pwd) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement link=locateElement("LinkText","CRM/SFA");
		click(link);
}
@AfterMethod(groups="any")
public void closeApp() {
	closeBrowser();
	
}
@AfterSuite(groups="any")
public void endTest() {
	endResult();
}
}

