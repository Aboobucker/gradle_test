  package week5.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Readexcel {
	
	public static Object[][] readData()  throws IOException {
		XSSFWorkbook Wb= new XSSFWorkbook("./Data/TC001createLead.xlsx");
		
		XSSFSheet sheet = Wb.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();//prints the row count
		System.out.println(rowcount);
		
		int cellcount = sheet.getRow(1).getLastCellNum();//prints the cell count
		System.out.println(cellcount);
		
		Object[][] data=new Object[rowcount][cellcount];
		for (int i = 1; i <=rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
			
			for (int j = 0; j <=cellcount; j++) {
				XSSFCell cell=row.getCell(j);
				
				try {
					String Value = cell.getStringCellValue();//print the all the data in excel
					System.out.println(Value);
					data[i-1][j]=Value;
				} catch (Exception e) {
					System.out.println("");
				}
			}
		}
		return null;
		
		
	}

}
